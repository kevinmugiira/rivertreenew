<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::resource ('about','AboutController');
Route::resource ('blog','BlogController');
Route::resource ('contact','ContactController');
Route::resource ('events','EventsController');
Route::resource ('excursions','ExcursionsController');
Route::resource ('gallery','GalleryController');
Route::resource ('hike','HikeController');
Route::resource ('ice','IceController');
Route::resource ('index','IndexController');
Route::resource ('master','MasterController');
Route::resource ('photography','PhotographyController');
Route::resource ('photos','PhotosController');
Route::resource ('private','PrivateController');
Route::resource ('stories','StoriesController');
Route::resource ('/','IndexController');