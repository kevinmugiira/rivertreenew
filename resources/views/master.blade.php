
<!DOCTYPE HTML>
<html>
<head>
    <title>River Tree</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="{{asset('asset/css/style.css')}}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{asset('asset/css/slider.css')}}" rel="stylesheet" type="text/css" media="all"/>
    <script type="text/javascript" src="{{asset('asset/js/jquery-1.9.0.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('asset/js/jquery.nivo.slider.js')}}"></script>
    <script type="text/javascript">
        $(window).load(function() {
            $('#slider').nivoSlider();
        });
    </script>
</head>
<body>
<div class="header">
    <div class="header_top">
        <div class="wrap">
            <div class="logo">
                <a href="{{url('index')}}"><img src="{{asset('asset/TEAMBUILDING PICS/The River Tree Logo.jpg')}}" alt="" /></a>
            </div>
            <div class="menu">
                <ul>
                    <li><a href="{{url('index')}}">HOME</a></li>
                    <li><a href="{{url('about')}}">ABOUT</a></li>
                    <li><a href="{{url('events')}}">EVENTS</a></li>
                    <li><a href="{{url('gallery')}}">GALLERY</a></li>
                    <!--<li><a href="{{url('blog')}}">BLOG</a></li>-->
                    <li><a href="{{url('contact')}}">CONTACT</a></li>
                    <div class="clear"></div>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    @yield('about')
    @yield('blog')
    @yield('contact')
    @yield('events')
    @yield('gall')
    @yield('index')
    @yield('photo')
    @yield('story')
    @yield('excur')
    @yield('hike')
    @yield('ice')
    @yield('pho')
    @yield('private')
    <div class="footer">
        <div class="wrap">
            <div class="half-footer" style="margin-left:0">
                <ul class="feeds">
                    <h3>Our Main Events</h3>
                    <li><a href="{{url('photos')}}">Team Building activities</a></li>
                    <li><a href="{{url('about')}}">Conference Solutions</a></li>
                    <li><a href="{{url('events')}}">Events we offer</a></li>
                    <!--<li><a href="{{url('blog')}}">Incentives we provide</a></li>-->
                    <li><a href="{{url('contact')}}">Our contacts</a></li>
                </ul>
                <div class="footer-pic"><img src="{{asset('asset/images/f-icon.png')}}" alt=""></div>
                <div class="clear"></div>
            </div>
            <div class="half-footer" style="border:none">
                <ul class="address">
                    <h3>Catch on</h3>
                    <li><a href="{{url('events')}}">Events</a></li>
                    <li><a href="{{url('photos')}}">Team buildings</a></li>
                    <!--<li><a href="{{url('blog')}}">Incentives</a></li>-->
                    <li><a href="https://web.facebook.com/RiverTreeC/">Facebook </a></li>
                    <li><a href="{{url('contact')}}">Twitter</a></li>
                </ul>
                <div class="footer-pic"><img src="{{asset('asset/images/foot-icon.png')}}'" alt=""></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
</body>
</html>

