@extends('master')



@section('pho')

    <div class="main">
        <div class="wrap">
            <div class="content_top">
                <div class="blog">
                    <h2>Photography</h2>
                    <div class="blog-leftgrids">
                        <div class="image group">
                            <div class="grid images_3_of_1">
                                <a href="#"><img src="{{asset('asset/TEAMBUILDING PICS/DSC00707.jpeg')}}" alt=""></a>
                            </div>
                            <div class="grid blog-desc">
                                <!--<h4><span>Photography</span></h4>
                                <h4>Sep 14TH, 2013,&nbsp;&nbsp; posted By <a href="#" class="post">Lorem ipsum</a></h4>-->
                                <p><span>We will provide a Branded background and photo props for this. A Professional photographer  will cover the days events and activities as well as evening. We will have an instant printer outside the venue to ensure photos are printed and circulated to the participants on site
    The day’s images can be projected on a projector screen during dinner. Photos of the event will be provided on DVD within the course of the following day.</span></p>
                                <!--<a href="#" class="button">Read More</a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection