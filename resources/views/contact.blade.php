@extends('master')




@section('contact')

    <div class="main">
        <div class="wrap">
            <div class="content_top">
                <div class="contact">
                    <div class="section group">
                        <div class="col_1_of_3 contact_1_of_3">
                            <div class="contact-form">
                                <h3>Get In Touch</h3>
                                <form>
                                    <div>
                                        <span><input type="text" class="textbox" value="Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}"></span>
                                    </div>
                                    <div>
                                        <span><input type="text" class="textbox" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}"></span>
                                    </div>
                                    <div>
                                        <span><textarea value="Message:" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message';}">Message</textarea></span>
                                    </div>
                                    <div>
                                        <span><input type="submit" class="mybutton" value="Submit"></span>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col_1_of_3 contact_1_of_3">
                            <div class="company_address">
                                <h3>Our contacts</h3>
                                <p>Local productions kenya</p>
                                <p>P.O Box 8160, 00100, Nairobi </p>
                                <p>KENYA</p>
                                <p>Phone Nick: +254 720 494 222 / 736 999 222</p>
                                <p>Email: <span><a href="#">E-Mail:
training@rivertreeltd.com <a href="#">marketing@rivertreeltd.com    <a href="#"> nickladu@rivertreeltd.com </a></span></p>
                                <p>Follow on: <span><a href="https://web.facebook.com/RiverTreeC/">Facebook</a></span></p>
                            </div>
                        </div>
                        <div class="col_1_of_3 contact_1_of_3">
                            <div class="contact_info">
                                <h3>Find Us Here</h3>
                                <div class="map">
                                    <iframe width="100%" height="250" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Lighthouse+Point,+FL,+United+States&amp;aq=4&amp;oq=light&amp;sll=26.275636,-80.087265&amp;sspn=0.04941,0.104628&amp;ie=UTF8&amp;hq=&amp;hnear=Lighthouse+Point,+Broward,+Florida,+United+States&amp;t=m&amp;z=14&amp;ll=26.275636,-80.087265&amp;output=embed"></iframe><br><small><a href="https://maps.google.co.in/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Lighthouse+Point,+FL,+United+States&amp;aq=4&amp;oq=light&amp;sll=26.275636,-80.087265&amp;sspn=0.04941,0.104628&amp;ie=UTF8&amp;hq=&amp;hnear=Lighthouse+Point,+Broward,+Florida,+United+States&amp;t=m&amp;z=14&amp;ll=26.275636,-80.087265" style="color:#F39EFF;;text-align:left;font-size:0.85em">View Larger Map</a></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection