@extends('master')




@section('photo')


    <div class="main">
        <div class="wrap">
            <div class="content_top">
                <div class="gallery">
                    <h3>Photos</h3>
                    <div class="section group">
                        <div class="grid_1_of_4 images_1_of_4">
                            <a href="{{asset('asset/TEAMBUILDING PICS/DSC00378.jpeg')}}" class="swipebox" title="Image Title"> <img src="{{asset('asset/TEAMBUILDING PICS/DSC00378.jpeg')}}" alt=""> <span class="zoom-icon"></span></a>
                        </div>
                        <div class="grid_1_of_4 images_1_of_4">
                            <a href="{{asset('asset/TEAMBUILDING PICS/DSC_0031.jpeg')}}" class="swipebox" title="Image Title"> <img src="{{asset('asset/TEAMBUILDING PICS/DSC_0031.jpeg')}}" alt=""><span class="zoom-icon"></span> </a>
                        </div>
                        <div class="grid_1_of_4 images_1_of_4">
                            <a href="{{asset('asset/TEAMBUILDING PICS/DSCF5200.jpeg')}}" class="swipebox" title="Image Title"> <img src="{{asset('asset/TEAMBUILDING PICS/DSCF5200.jpeg')}}" alt=""><span class="zoom-icon"></span> </a>
                        </div>
                        <div class="grid_1_of_4 images_1_of_4">
                            <a href="{{asset('asset/TEAMBUILDING PICS/DSCF6938.jpeg')}}" class="swipebox" title="Image Title"> <img src="{{asset('asset/TEAMBUILDING PICS/DSCF6938.jpeg')}}" alt=""><span class="zoom-icon"></span> </a>
                        </div>
                    </div>
                    <div class="section group">
                        <div class="grid_1_of_4 images_1_of_4">
                            <a href="{{asset('asset/TEAMBUILDING PICS/DSCF7174.jpeg')}}" class="swipebox" title="Image Title"> <img src="{{asset('asset/TEAMBUILDING PICS/DSCF7174.jpeg')}}" alt=""><span class="zoom-icon"></span> </a>
                        </div>
                        <div class="grid_1_of_4 images_1_of_4">
                            <a href="{{asset('asset/TEAMBUILDING PICS/resized_resized_DSC_8109.jpeg')}}" class="swipebox" title="Image Title"> <img src="{{asset('asset/TEAMBUILDING PICS/resized_resized_DSC_8109.jpeg')}}" alt=""><span class="zoom-icon"></span> </a>
                        </div>
                        <div class="grid_1_of_4 images_1_of_4">
                            <a href="{{asset('asset/TEAMBUILDING PICS/DSC_1680.jpeg')}}" class="swipebox" title="Image Title"> <img src="{{asset('asset/TEAMBUILDING PICS/DSC_1680.jpeg')}}" alt=""><span class="zoom-icon"></span> </a>
                        </div>
                        <div class="grid_1_of_4 images_1_of_4">
                            <a href="{{asset('asset/TEAMBUILDING PICS/DSC_0387.jpeg')}}" class="swipebox" title="Image Title"> <img src="{{asset('asset/TEAMBUILDING PICS/DSC_0387.jpeg')}}" alt=""><span class="zoom-icon"></span> </a>
                        </div>
                    </div>
                    <!--<div class="projects-bottom-paination">
                        <ul>
                            <li class="active"><a href="/#">1</a></li>
                            <li><a href="#">2</a></li>
                        </ul>
                    </div>-->
                </div>
            </div>
        </div>
    </div>




@endsection