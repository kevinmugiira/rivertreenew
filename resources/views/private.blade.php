@extends('master')



@section('private')

    <div class="main">
        <div class="wrap">
            <div class="content_top">
                <div class="blog">
                    <h2><span>Private events</span></h2>
                    <div class="blog-leftgrids">
                        <div class="image group">
                            <div class="grid images_3_of_1">
                                <a href="#"><img src="{{asset('asset/TEAMBUILDING PICS/DSC00605.jpeg')}}" alt=""></a>
                            </div>
                            <div class="grid blog-desc">
                                <!--<h4><span>Private events</span></h4>
                                <h4>Sep 14TH, 2013,&nbsp;&nbsp; posted By <a href="#" class="post">Lorem ipsum</a></h4>-->
                                <p><span>Birthday parties, weddings, anniversaries, traditional ceremonies and any other domestic event, we service fully</span></p>
                                <!--<a href="#" class="button">Read More</a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection