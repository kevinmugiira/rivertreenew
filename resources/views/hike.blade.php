@extends('master')




@section('hike')

    div class="main">
    <div class="wrap">
        <div class="content_top">
            <div class="blog">
                <h2>Hike retreat programs</h2>
                <div class="blog-leftgrids">
                    <div class="image group">
                        <div class="grid images_3_of_1">
                            <a href="#"><img src="{{asset('asset/TEAMBUILDING PICS/DSC_1624.jpeg')}}" alt=""></a>
                        </div>
                        <div class="grid blog-desc">
                            <!--<h4><span>Hike retreat programs</span></h4>
                            <h4>Sep 14TH, 2013,&nbsp;&nbsp; posted By <a href="#" class="post">Lorem ipsum</a></h4>-->
                            <p><span>Morning session
9am Breakfast
            Hike on Mt. Kilimambogo
1pm Lunch
2pm fun games/ Childhood games/ cook-off, Zumba fun dance, party games, board games
     Drinks served, cocktail making and disco music plays
5pm Final debrief and return to Nairobi</span></p>
                            <!--<a href="#" class="button">Read More</a>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection