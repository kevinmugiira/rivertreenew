@extends('master')



@section('gall')

    <div class="main">
        <div class="wrap">
            <div class="content_top">
                <div class="gallery">
                    <h3>Gallery</h3>
                    <div class="section group">
                        <div class="grid_1_of_4 images_1_of_4">
                            <a href="{{asset('asset/TEAMBUILDING PICS/DSC_1559.jpeg')}}" class="swipebox" title="Image Title"> <img src="{{asset('asset/TEAMBUILDING PICS/DSC_1559.jpeg')}}" alt=""> <span class="zoom-icon"></span></a>
                        </div>
                        <div class="grid_1_of_4 images_1_of_4">
                            <a href="{{asset('asset/TEAMBUILDING PICS/DSC_3660.jpeg')}}" class="swipebox" title="Image Title"> <img src="{{asset('asset/TEAMBUILDING PICS/DSC_3660.jpeg')}}" alt=""><span class="zoom-icon"></span> </a>
                        </div>
                        <div class="grid_1_of_4 images_1_of_4">
                            <a href="{{asset('asset/TEAMBUILDING PICS/DSC_4904.jpeg')}}" class="swipebox" title="Image Title"> <img src="{{asset('asset/TEAMBUILDING PICS/DSC_4904.jpeg')}}" alt=""><span class="zoom-icon"></span> </a>
                        </div>
                        <div class="grid_1_of_4 images_1_of_4">
                            <a href="{{asset('asset/TEAMBUILDING PICS/DSCF6745.jpeg')}}" class="swipebox" title="Image Title"> <img src="{{asset('asset/TEAMBUILDING PICS/DSCF6745.jpeg')}}" alt=""><span class="zoom-icon"></span> </a>
                        </div>
                    </div>
                    <div class="section group">
                        <div class="grid_1_of_4 images_1_of_4">
                            <a href="{{asset('asset/TEAMBUILDING PICS/DSCF6925.jpeg')}}" class="swipebox" title="Image Title"> <img src="{{asset('asset/TEAMBUILDING PICS/DSCF6925.jpeg')}}" alt=""><span class="zoom-icon"></span> </a>
                        </div>
                        <div class="grid_1_of_4 images_1_of_4">
                            <a href="{{asset('asset/TEAMBUILDING PICS/DSCF7235.jpeg')}}" class="swipebox" title="Image Title"> <img src="{{asset('asset/TEAMBUILDING PICS/DSCF7235.jpeg')}}" alt=""><span class="zoom-icon"></span> </a>
                        </div>
                        <div class="grid_1_of_4 images_1_of_4">
                            <a href="{{asset('asset/TEAMBUILDING PICS/IMG_2591.jpeg')}}" class="swipebox" title="Image Title"> <img src="{{asset('asset/TEAMBUILDING PICS/IMG_2591.jpeg')}}" alt=""><span class="zoom-icon"></span> </a>
                        </div>
                        <div class="grid_1_of_4 images_1_of_4">
                            <a href="{{asset('asset/TEAMBUILDING PICS/IMG_2797.jpeg')}}" class="swipebox" title="Image Title"> <img src="{{asset('asset/TEAMBUILDING PICS/IMG_2797.jpeg')}}" alt=""><span class="zoom-icon"></span> </a>
                        </div>
                    </div>
                    <div class="projects-bottom-paination">
                        <ul>
                            <li class="active"><a href="/#">1</a></li>
                            <li><a href="{{url('photos')}}">2</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection