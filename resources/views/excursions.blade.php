@extends('master')



@section('excur')

    <div class="main">
        <div class="wrap">
            <div class="content_top">
                <div class="blog">
                    <h2>Excursions</h2>
                    <div class="blog-leftgrids">
                        <div class="image group">
                            <div class="grid images_3_of_1">
                                <a href="#"><img src="{{asset('asset/TEAMBUILDING PICS/DSC_9493.jpeg')}}" alt=""></a>
                            </div>
                            <div class="grid blog-desc">
                                <!--<h4><span>Excursions</span></h4>
                                <h4>Sep 14TH, 2013,&nbsp;&nbsp; posted By <a href="#" class="post">Lorem ipsum</a></h4>-->
                                <p><span>On road and off road hikes
Boat rides
Cycling
Picnics and Barbecues
Charity retreats
Adventure retreats (white water rafting, rock climbing, zip lining, bunjee jumping, archery, mud races, Camping)
Treasure hunt races
Waterpark fun retreats
Marine park exploration and Snorkeling</span></p>
                                <!--<a href="#" class="button">Read More</a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection