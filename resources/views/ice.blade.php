@extends('master')




@section('ice')

    <div class="main">
        <div class="wrap">
            <div class="content_top">
                <div class="blog">
                    <h2>Ice breakers and fun dances</h2>
                    <div class="blog-leftgrids">
                        <div class="image group">
                            <div class="grid images_3_of_1">
                                <a href="#"><img src="{{asset('asset/TEAMBUILDING PICS/DSC00197.jpeg')}}" alt=""></a>
                            </div>
                            <div class="grid blog-desc">
                                <!--<h4><span>Ice breakers and fun dances</span></h4>
                                <h4>Sep 14TH, 2013,&nbsp;&nbsp; posted By <a href="#" class="post">Lorem ipsum</a></h4>-->
                                <p><span>This will be the day’s highlight just after Lunch.  It will comprise a mix of high energy races, Fun games, wet activities session and an optional childhood fun games session.
This will be followed by a final discussion of the day that will challenge individuals to draw lessons that they can take back to work with them and an action plan as well.</span></p>
                                <!--<a href="#" class="button">Read More</a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection