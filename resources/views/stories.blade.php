@extends('master')



@section('story')

    <div class="main">
        <div class="wrap">
            <div class="content_top">
                <div class="blog">
                    <h2>Jitambulishe group fun games</h2>
                    <div class="blog-leftgrids">
                        <div class="image group">
                            <div class="grid images_3_of_1">
                                <a href="#"><img src="{{asset('asset/TEAMBUILDING PICS/DSC_1553.jpeg')}}" alt=""></a>
                            </div>
                            <div class="grid blog-desc">
                                <!--<h4><span>Jitambulishe group fun games</span></h4>-->
                                <!--<h4>Sep 14TH, 2013,&nbsp;&nbsp; posted By <a href="#" class="post">Lorem ipsum</a></h4>-->
                                <p><span>Engaging participants in team formation, Creation of a team symbol, customer focused values and team motto, creating a team identity that resonates with the organizations vision and mission. Main activities include flag making, Talent search. etc.These group fun games will challenge participants  and their ability to work as a team on different tasks. The main activities include the toxic river, Juggling, memory game, Robbing the bank, 15 pieces. etc</span></p>
                                <!--<a href="#" class="button">Read More</a>-->
                            </div>
                        </div>
                    </div>

                    <!--<div class="content-pagenation">
                        <ul>
                            <li><a href="#">Frist</a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><span>....</span></li>
                            <li><a href="#">Last</a></li>
                        </ul>
                        <div class="clear"> </div>
                    </div>-->
                </div>
            </div>
        </div>
    </div>


@endsection