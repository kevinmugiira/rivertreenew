@extends('master')



@section('about')



    <div class="main">
        <div class="wrap">
            <div class="content_top">
                <div class="section group">
                    <div class="col_1_of_3 span_1_of_3">
                        <h2><span>Our team</span></h2>
                        <h4>Meet Nick</h4>
                        <p>Nick brings on board  7 years of Hotel Management experience from Sarova Hotels with practical hands on skills that were gained planning  and facilitating teambuilding retreats and corporate  events for one of the regions' largest 5-star beach Resorts.
                            He is an experienced business man and has served in various capacities as an Entertainment Manager, Afro carribean dance trainer, Corporate event Emcee and is an avid adventure junkie and motorcycle enthusiast. He is also a corporate staff motivation trainer with over 9 years training experience and hundreds of teams successfully trained. </p>
                        <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>-->
                        <div class="event-img">
                            <img src="{{asset('asset/TEAMBUILDING PICS/DSC_3670.jpeg')}}"/>
                        </div>
                    </div>
                    <div class="col_1_of_3 span_1_of_3">
                        <div class="menu_timmings">
                            <ul>
                                <li>
                                    <div class="txt1">The members</div>
                                    <p><li>SERRAINE NYAMORI – Organizational Development expert</li>
                                <li>MOSES OBILO – Business Development and Lead Facilitator-Mombasa, Kilifi and Malindi.</li>
                                </p>

                                <p><li>WANI WELIKHE - Business Development and Logistics Manager.</li>
                                <li>PETER MUTHIGANI –Business Development and Lead facilitator – Nairobi.</li></p>


                                <p><li>JOHN ORIARO – Lead Facilitator and Photographer - Nairobi.</li>
                                <li>FABBY AWUOR – Lead Facilitator and Fitness coach – Nairobi.</li></p>

                                </li>

                            </ul>
                        </div>
                    </div>
                    <div class="col_1_of_3 span_1_of_3">
                        <h2><span>Services we offer</span></h2>
                        <div class="event-grid">
                            <div class="event_img">
                                <img src="{{asset('asset/TEAMBUILDING PICS/DSC00380.jpeg')}}" title="post1" alt="">
                            </div>
                            <div class="event_desc">
                                <h4><span>TEAMBUILDING FACILITATION</span></h4>
                                <!--<h4>Aug 28Th, 2013</h4>-->
                                <p>Our programs range from ½ day, full day, multiple day both indoor and outdoor fun programs.
                                    Our programs also target different focus groups including adults, teens and Kids.
                                    Our Range of clientele includes, corporate organizations, schools and institutions, NGO’s, Government agencies, Private firms, Business enterprises and friends/ family gatherings.<a href="{{url('events')}}">[...]</a></p>
                            </div>
                            <div class="clear"> </div>
                        </div>
                        <div class="event-grid">
                            <div class="event_img">
                                <img src="{{asset('asset/TEAMBUILDING PICS/DSC_0095.jpeg')}}" title="post1" alt="">
                            </div>
                            <div class="event_desc">
                                <h4><span>GROUP MEETING FACILITATION</span></h4>
                                <!--<h4>Aug 2ND, 2013</h4>-->
                                <p>We specialize in Conference Moderation, strategic planning meetings, facilitating AGM Meetings, Project closure meetings, Chama meetings, etc<a href="{{url('events')}}">[...]</a></p>
                            </div>
                            <div class="clear"> </div>
                        </div>
                        <div class="event-grid">
                            <div class="event_img">
                                <img src="{{asset('asset/TEAMBUILDING PICS/resized_DSCF4233.jpeg')}}" title="post1" alt="">
                            </div>
                            <div class="event_desc">
                                <h4><span>CORPORATE EVENTS</span></h4>
                                <!--<h4>july 28Th, 2013</h4>-->
                                <p>We offer professional, meticulous and experienced services in event management, coordination and supplies for all your event needs<a href="{{url('events')}}">[...]</a></p>
                            </div>
                            <div class="clear"> </div>
                        </div>
                        <div class="view-all"><a href="{{url('events')}}">ViewAll</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection