@extends('master')



@section('events')

    <div class="main">
        <div class="wrap">
            <div class="content_top">
                <div class="events">
                    <h2>Events we offer</h2>
                    <div class="section group">
                        <div class="grid_1_of_3 events_1_of_3">
                            <div class="event-time">
                                <h4><span>Excursions</span></h4>
                                <!--<h4>Sep 20Th, 2013</h4>-->
                            </div>
                            <div class="event-img">
                                <a href="{{url('excursions')}}"><img src="{{asset('asset/TEAMBUILDING PICS/DSC_9485.jpeg')}}" alt=""><span>Read More</span></a>
                            </div>
                        </div>
                        <div class="grid_1_of_3 events_1_of_3">
                            <div class="event-time">
                                <h4><span>Hike retreat program</span></h4>
                                <!--<h4>Sep 14TH, 2013</h4>-->
                            </div>
                            <div class="event-img">
                                <a href="{{url('hike')}}"><img src="{{asset('asset/TEAMBUILDING PICS/DSC_9509.jpeg')}}" alt=""><span>Read More</span></a>
                            </div>
                        </div>
                        <div class="grid_1_of_3 events_1_of_3">
                            <div class="event-time">
                                <h4><span>Private events</span></h4>
                                <!--<h4>Sep 5TH, 2013</h4>-->
                            </div>
                            <div class="event-img">
                                <a href="{{url('private')}}"><img src="{{asset('asset/TEAMBUILDING PICS/DSC00605.jpeg')}}" alt=""><span>Read More</span></a>
                            </div>
                        </div>
                    </div>
                    <div class="section group">
                        <div class="grid_1_of_3 events_1_of_3">
                            <div class="event-time">
                                <h4><span>Ice breakers and fun Dances</span></h4>
                                <!--<h4>Sep 20Th, 2013</h4>-->
                            </div>
                            <div class="event-img">
                                <a href="{{url('ice')}}"><img src="{{asset('asset/TEAMBUILDING PICS/DSC00197-(2).jpeg')}}" alt=""><span>Read More</span></a>
                            </div>
                        </div>
                        <div class="grid_1_of_3 events_1_of_3">
                            <div class="event-time">
                                <h4><span>Jitambulishe group fun games</span></h4>
                                <!--<h4>Sep 14TH, 2013</h4>-->
                            </div>
                            <div class="event-img">
                                <a href="{{url('stories')}}"><img src="{{asset('asset/TEAMBUILDING PICS/DSC_1553.jpeg')}}" alt=""><span>Read More</span></a>
                            </div>
                        </div>
                        <div class="grid_1_of_3 events_1_of_3">
                            <div class="event-time">
                                <h4><span>Photography</span></h4>
                                <!--<h4>Sep 5TH, 2013</h4>-->
                            </div>
                            <div class="event-img">
                                <a href="{{url('photography')}}"><img src="{{asset('asset/TEAMBUILDING PICS/DSC00707.jpeg')}}" alt=""><span>Read More</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection