@extends('master')



@section('index')

    <!------ Slider ------------>
    <div class="wrap">
        <div class="slider">
            <div class="slider-wrapper theme-default">
                <div id="slider" class="nivoSlider">
                    <img src="{{asset('asset/TEAMBUILDING PICS/AT PRIDEINN PARADISE BEACH RESORT TEAM BUILDING & DINNER 2ND APRIL 2016 (50)-Recovered.jpg')}}" data-thumb="{{asset('asset/TEAMBUILDING PICS/AT PRIDEINN PARADISE BEACH RESORT TEAM BUILDING & DINNER 2ND APRIL 2016 (50)-Recovered.jpg')}}" alt="" />
                    <img src="{{asset('asset/TEAMBUILDING PICS/AT PRIDEINN PARADISE BEACH RESORT TEAM BUILDING & DINNER 2ND APRIL 2016 (59)-Recovered.jpg')}}" data-thumb="{{asset('asset/TEAMBUILDING PICS/AT PRIDEINN PARADISE BEACH RESORT TEAM BUILDING & DINNER 2ND APRIL 2016 (59)-Recovered.jpg')}}" alt="" />
                    <img src="{{asset('asset/TEAMBUILDING PICS/DSC_0501-Recovered.jpg')}}" data-thumb="{{asset('asset/TEAMBUILDING PICS/DSC_0501-Recovered.jpg')}}" alt="" />
                    <img src="{{asset('asset/TEAMBUILDING PICS/resized_2(253)-Recovered.jpg')}}" data-thumb="{{asset('asset/TEAMBUILDING PICS/resized_2(253)-Recovered.jpg')}}" alt="" />
                    <img src="{{asset('asset/TEAMBUILDING PICS/resized_2 (124)-Recovered.jpg')}}" data-thumb="{{asset('asset/TEAMBUILDING PICS/resized_2 (124)-Recovered.jpg')}}" alt="" />
                </div>
            </div>
        </div>
    </div>
    <!------End Slider ------------>
    </div>
    <div class="main">
        <div class="wrap">
            <div class="section group">
                <div class="listview_1_of_3 images_1_of_3 event_grid">
                    <a href="{{url('events')}}">
                        <div class="listimg listimg_1_of_2">
                            <img src="{{asset('asset/TEAMBUILDING PICS/DSC00378.jpeg')}}" alt="" />
                        </div>
                        <!--<div class="text list_1_of_2">
                            <div class="date">
                                <figure><span>02</span>January</figure>
                            </div>
                        </div>-->
                    </a>
                </div>
                <div class="listview_1_of_3 images_1_of_3 event_grid">
                    <a href="{{url('events')}}">
                        <div class="listimg listimg_1_of_2">
                            <img src="{{asset('asset/TEAMBUILDING PICS/DSC00197.jpeg')}}" alt="" />
                        </div>
                        <!--<div class="text list_1_of_2">
                            <div class="date">
                                <figure><span>14</span>March</figure>
                            </div>
                        </div>-->
                    </a>
                </div>
                <div class="listview_1_of_3 images_1_of_3 event_grid">
                    <a href="{{url('events')}}">
                        <div class="listimg listimg_1_of_2">
                            <img src="{{asset('asset/TEAMBUILDING PICS/DSC_0279.jpeg')}}" alt="" />
                        </div>
                        <!--<div class="text list_1_of_2">
                            <div class="date">
                                <figure><span>18</span>March</figure>
                            </div>-->
                </div>
                </a>
            </div>
        </div>
        <div class="content_bottom">
            <div class="section group">
                <div class="col_1_of_3 span_1_of_3 today_show">
                    <h3>The best,just for you</h3>
                    <img src="{{asset('asset/TEAMBUILDING PICS/DSC_0399.jpeg')}}" alt="" />
                    <!--<h5><span>31 March</span></h5>-->
                    <h5>A small glance of the quisine offered in the places we take our guests. </h5>
                    <a href="#" class="button">View More</a>
                </div>
                <div class="col_1_of_3 span_1_of_3">
                    <h3>Who we are </h3>
                    <div class="track_list">
                        <p>THE RIVER TREE COMPANY is a staff motivation and Event Management Firm based in Nairobi, Kenya. Our vision is to convert every staff related event into a motivation tool to ensure an optimum work environment, associates who are fully engaged at work and a thriving business.  At the bedrock of our engagement is a client centric approach to enriching Organizations, Businesses and Institutions towards long term sustainability and key focus on Staff Productivity, Organization Profitability and Social Responsibility & Accountability.
                            THE RIVER TREE COMPANY was established with a vision to give every Organization’s, Business’ and Institution’s event a fingerprint experience and keep it alive non-stop to the end. To achieve this we endeavor to deliver of a series of once-in-a-lifetime memories, stitched together by meticulous processes and seasoned with unrelenting professionalism.
                            With over 10 years experience in events, training and hospitality management, and a reach of over 500 different events to our name, our mission is to deliver an event with audacious creativity and spot-on execution while employing every resource at our disposal.</p>

                    </div>
                </div>
                <div class="col_1_of_3 span_1_of_3">
                    <h3>Group events</h3>
                    <a href="{{url('gallery')}}"><img src="{{asset('asset/TEAMBUILDING PICS/DSC_1559.jpeg')}}" alt="" /></a>
                    <p>Group members having fun in one of the group activity events that we offer. </p>
                    <a href="{{url('gallery')}}" class="button">See All</a>
                </div>
            </div>
        </div>
    </div>
@endsection